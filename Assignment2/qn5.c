#include <stdio.h>

#define MAX_SIZE 1000

int is_sorted(int arr[], int size) {
    int i;
    int order = (arr[0] < arr[1])? -1 : 1;   // 1 means decreasing
    int new_order;
    for(i = 2; i < size; ++i) {
        new_order = (arr[i-1] < arr[i])? -1: 1;
        if(order != new_order)
            return 0;
        order = new_order;
    }
    return order;
}

int main() {
    int arr[MAX_SIZE];
    int size;
    printf("Enter number of elements: ");
    scanf("%d", &size);
    int i;
    for(i = 0; i < size; ++i)
        scanf("%d", &arr[i]);
    int order = is_sorted(arr, size);
    if(order) {
        printf("Array is sorted.\n");
        if(order == 1)
            printf("Order: Decreasing.\n");
        else
            printf("Order: Increasing.\n");
    }
    else
        printf("Array is not sorted.\n");
    return 0;
}