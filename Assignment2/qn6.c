#include <stdio.h>

#define MAX_SIZE 100

void init_arr(int arr[], int size) {
    int i;
    for(i = 0; i < size; ++i)
        arr[i] = 0;
}

int is_sorted(int arr[], int size) {
    int i;
    int order = (arr[0] < arr[1])? -1 : 1;   // 1 means decreasing
    int new_order;
    for(i = 2; i < size; ++i) {
        new_order = (arr[i-1] < arr[i])? -1: 1;
        if(order != new_order)
            return 0;
        order = new_order;
    }
    return order;
}

void print_order(int order) {
    if(order) {
        printf("Array is sorted.\n");
        if(order == 1)
            printf("Order: Decreasing.\n");
        else
            printf("Order: Increasing.\n");
    }
    else
        printf("Array is not sorted.\n");
}

int compare(int num1, int num2, int order) {
    if(order == 1)
        return num1 > num2;
    else
        return num1 < num2;
}

void merge_arr(int dest[], int arr1[], int size1, int arr2[], int size2) {
    int order1 = is_sorted(arr1, size1);
    printf("Array1: ");
    print_order(order1);
    int order2 = is_sorted(arr2, size2);
    printf("Array2: ");
    print_order(order2);
    if(order1 == order2 && order1 != 0) {
        printf("Order matched. Merging now...\n");
        int pos1 = 0, pos2 = 0;
        int i;
        for(i = 0; (i < size1 + size2) && (pos1 < size1 && pos2 < size2); ++i) {
            if(compare(arr1[pos1], arr2[pos2], order1)) {
                dest[i] = arr1[pos1];
                ++pos1;
            }
            else {
                dest[i] = arr2[pos2];
                ++pos2;
            }
        }
        if(pos1 == size1) {
            while(pos2 < size2) {
                dest[i] = arr2[pos2];
                ++pos2;
                ++i;
            }
        }
        else if(pos2 == size2) {
            while(pos1 < size1) {
                dest[i] = arr1[pos1];
                ++pos1;
                ++i;
            }
        }
        return;
    }
    else {
        printf("Order mismatch. Can't merge arrays.\n");
        return;
    }
}

int main() {
    int arr1[MAX_SIZE];
    init_arr(arr1, MAX_SIZE);
    int size1;
    printf("Enter number of elements in array1: ");
    scanf("%d", &size1);
    int i;
    printf("Enter elements:\n");
    for(i = 0; i < size1; ++i)
        scanf("%d", &arr1[i]);
    
    int arr2[MAX_SIZE];
    init_arr(arr2, MAX_SIZE);
    int size2;
    printf("Enter number of elements in array2: ");
    scanf("%d", &size2);
    printf("Enter elements:\n");
    for(i = 0; i < size2; ++i)
        scanf("%d", &arr2[i]);
    
    int dest[2 * MAX_SIZE];
    init_arr(dest, 2 * MAX_SIZE);
    merge_arr(dest, arr1, size1, arr2, size2);

    printf("\n  Merged array: \n");
    for(i = 0; i < size1 + size2; ++i) {
        printf("  %d  ", dest[i]);
    }

    return 0;
}