#include <stdio.h>
#include <stdlib.h>

#define MAX_SIZE 1000

typedef struct {
    int arr[MAX_SIZE + 1];
} list;

void init(list *l) {
    l->arr[MAX_SIZE] = 0;
}

int is_empty(list *l) {
    if(l->arr[MAX_SIZE] == 0)
        return 1;
    else
        return 0;
}

int size(list *l) {
    return l->arr[MAX_SIZE];
}

void read(list *l, int n) {
    if(n < l->arr[MAX_SIZE]) {
        printf("%d\n", l->arr[n]);
    }
}

void write(list *l, int d) {
    l->arr[l->arr[MAX_SIZE]] = d;
    l->arr[MAX_SIZE] += 1;
}

void insert_n(list *l, int n, int d) {
    int temp = l->arr[MAX_SIZE];
    while(temp >= n) {
        l->arr[temp] = l->arr[temp - 1];
        --temp;
    }
    l->arr[n] = d;
    l->arr[MAX_SIZE] += 1;
}

void delete(list *l, int n) {
    int temp = l->arr[MAX_SIZE] - 1;
    while(temp >= n) {
        l->arr[temp-1] = l->arr[temp];
        --temp;
    }
}

int search(list *l, int key) {
    int i;
    for(i = 0; i < l->arr[MAX_SIZE]; ++l) {
        if(l->arr[i] == key)
            return 1;
    }
    return 0;
}

void sort(list *l) {
    int i, j;
    for(i = 0; i < l->arr[MAX_SIZE]; ++i) {
        int temp = l->arr[i];
        int pos = i;
        for(j = i + 1; j < l->arr[MAX_SIZE]; ++j) {
            if(l->arr[j] < temp) {
                pos = j;
                temp = l->arr[j];
            }
        }
        l->arr[pos] = l->arr[i];
        l->arr[i] = temp;
    }
}



int main() {
    list *l = malloc(sizeof(list));
    init(l);
    int i = 0;
    for(i = 0; i < 10; ++i) {
        write(l, 10-i+1);
    }
    for(i = 0; i < 10; ++i) {
        read(l, i);
    }
    printf("\nSorting now: \n");
    sort(l);
    for(i = 0; i < 10; ++i) {
        read(l, i);
    }
    return 0;
}