#include <stdio.h>

#define MAX_TERMS 1000
#define MAX_POLYNOMIALS 200

typedef struct {
    int expo;
    double coef;
} term;

typedef struct {
    int start;
    int end;
} polynomial;

term poly_terms[MAX_TERMS];
polynomial poly_marker[MAX_POLYNOMIALS];
int avail = 0;

void sort(int *arr, int size) {
    int i, j;
    for(i = 0; i < size-1; ++i) {
        int max = arr[i];
        int pos = i;
        for(j = i+1; j < size; ++j) {
            if(max < arr[j]) {
                max = arr[j];
                pos = j;
            }
        }
        int temp = arr[i];
        arr[i] = arr[pos];
        arr[pos] = temp;
    }
}

int is_zero(int index) {
    if(poly_terms[poly_marker[index].start].expo == 0 && poly_terms[poly_marker[index].start].coef == 0) {
        return 1;
    }
    return 0;
}


void print_poly(int index) {
    if(is_zero(index)) {
        printf("\n  Polynomial is zero polynomial.\n");
        return;
    }
    int startA = poly_marker[index].start,
        endA = poly_marker[index].end;
    if(startA < 0)
        return;
    printf("\n  Polynomial: ");
    printf("(%lf)x^(%d)", poly_terms[startA].coef, poly_terms[startA].expo);
    ++startA;
    while(startA <= endA) {
        printf(" + (%lf)x^(%d)", poly_terms[startA].coef, poly_terms[startA].expo);
        ++startA;
    }
    printf("\n");
}

int input_poly() {
    printf("\n  Enter number of terms: ");
    int n;
    scanf("%d", &n);
    if(n > (MAX_TERMS - poly_marker[avail-1].end)) {
        printf("%d < %d - %d", n, MAX_TERMS, avail);
        printf("\nNot enough space to accomodate polynolial of %d terms.", n);
        return -1;
    }
    if(n == 0) {        // Zero Polynomial
        poly_marker[avail].start = poly_marker[avail-1].end + 1;
        poly_marker[avail].end = poly_marker[avail].start;
        poly_terms[poly_marker[avail].start].expo = 0;
        poly_terms[poly_marker[avail].start].coef = 0;
        printf("  Saved as zero polynomial.\n");
        print_poly(avail);
        ++avail;
        return avail-1;
    }
    poly_marker[avail].start = avail;
    poly_marker[avail].end = avail + n - 1;
    int ctr = 0;
    // printf("   Enter terms in decreasing order of exponents:");
    printf("  Enter the exponents of all the terms of the polynomial: ");
    int ex[n];
    int i = 0;
    for(; i < n; ++i) {
        scanf("%d", &ex[i]);
        // printf("  -%d-  ", ex[i]);
    }
    sort(ex, n);
    // printf("\n");
    // for(i = 0; i < n; ++i) {
    //     printf(" %d ", ex[i]);
    // }
    // printf("\n");
    for(i = 0; i < n; ++i) {
        printf("    For exponent %d enter Coef: ", ex[i]);
        scanf("%lf", &poly_terms[poly_marker[avail].start + ctr].coef);
        poly_terms[poly_marker[avail].start + ctr++].expo = ex[i];
    }
    print_poly(avail);
    ++avail;
    return avail-1;
}

int add_poly(int poly1, int poly2) {
    int startA = poly_marker[poly1].start;
    int endA = poly_marker[poly1].end;
    int startB = poly_marker[poly2].start;
    int endB = poly_marker[poly2].end;
    int startRes = poly_marker[avail-1].end + 1;
    int ctr = 0;
    if(is_zero(poly1)) {
        while(startB <= endB) {
            poly_terms[startRes + ctr].expo = poly_terms[startB].expo;
            poly_terms[startRes + ctr].coef = poly_terms[startB].coef;
            ++startB;
            ++ctr;
        }
    }
    else if(is_zero(poly2)) {
        while(startA <= endA) {
            poly_terms[startRes + ctr].expo = poly_terms[startA].expo;
            poly_terms[startRes + ctr].coef = poly_terms[startA].coef;
            ++startA;
            ++ctr;
        }
    }
    else {
        while(startA <= endA && startB <= endB) {
            if(poly_terms[startA].expo < poly_terms[startB].expo) {
                poly_terms[startRes + ctr].expo = poly_terms[startB].expo;
                poly_terms[startRes + ctr].coef = poly_terms[startB].coef;
                ++startB;
                ++ctr;
            }
            else if(poly_terms[startA].expo > poly_terms[startB].expo) {
                poly_terms[startRes + ctr].expo = poly_terms[startA].expo;
                poly_terms[startRes + ctr].coef = poly_terms[startA].coef;
                ++startA;
                ++ctr;
            }
            else {
                poly_terms[startRes + ctr].expo = poly_terms[startA].expo;
                poly_terms[startRes + ctr].coef = poly_terms[startB].coef + poly_terms[startA].coef;
                ++startB;
                ++startA;
                ++ctr;
            }
        }
        while(startA <= endA) {
                poly_terms[startRes + ctr].expo = poly_terms[startA].expo;
                poly_terms[startRes + ctr].coef = poly_terms[startA].coef;
                ++startA;
                ++ctr;
        }
        while(startB <= endB) {
                poly_terms[startRes + ctr].expo = poly_terms[startB].expo;
                poly_terms[startRes + ctr].coef = poly_terms[startB].coef;
                ++startB;
                ++ctr;
        }
    }
    int endRes = startRes + ctr;
    poly_marker[avail].start = startRes;
    poly_marker[avail].end = endRes;
    ++avail;
    return avail-1;
}

int sub_poly(int poly1, int poly2) {
    int startA = poly_marker[poly1].start;
    int endA = poly_marker[poly1].end;
    int startB = poly_marker[poly2].start;
    int endB = poly_marker[poly2].end;
    int startRes = poly_marker[avail-1].end + 1;
    int ctr = 0;
    if(is_zero(poly1)) {
        while(startB <= endB) {
            poly_terms[startRes + ctr].expo = poly_terms[startB].expo;
            poly_terms[startRes + ctr].coef = - poly_terms[startB].coef;
            ++startB;
            ++ctr;
        }
    }
    else if(is_zero(poly2)) {
        while(startA <= endA) {
            poly_terms[startRes + ctr].expo = poly_terms[startA].expo;
            poly_terms[startRes + ctr].coef = poly_terms[startA].coef;
            ++startA;
            ++ctr;
        }
    }
    else {
        while(startA <= endA && startB <= endB) {
            if(poly_terms[startA].expo < poly_terms[startB].expo) {
                poly_terms[startRes + ctr].expo = poly_terms[startB].expo;
                poly_terms[startRes + ctr].coef = - poly_terms[startB].coef;
                ++startB;
                ++ctr;
            }
            else if(poly_terms[startA].expo > poly_terms[startB].expo) {
                poly_terms[startRes + ctr].expo = poly_terms[startA].expo;
                poly_terms[startRes + ctr].coef = poly_terms[startA].coef;
                ++startA;
                ++ctr;
            }
            else {
                if((poly_terms[startA].coef - poly_terms[startB].coef) != 0) {
                    poly_terms[startRes + ctr].expo = poly_terms[startA].expo;
                    poly_terms[startRes + ctr].coef = poly_terms[startA].coef - poly_terms[startB].coef;
                    ++ctr;
                }
                ++startB;
                ++startA;
            }
        }
        while(startA <= endA) {
                poly_terms[startRes + ctr].expo = poly_terms[startA].expo;
                poly_terms[startRes + ctr].coef = poly_terms[startA].coef;
                ++startA;
                ++ctr;
        }
        while(startB <= endB) {
                poly_terms[startRes + ctr].expo = poly_terms[startB].expo;
                poly_terms[startRes + ctr].coef = - poly_terms[startB].coef;
                ++startB;
                ++ctr;
        }
    }
    int endRes = startRes + ctr;
    poly_marker[avail].start = startRes;
    poly_marker[avail].end = endRes;
    ++avail;
    return avail-1;
}

int mult_const(int index, int k) {
    int start = poly_marker[index].start;
    int end = poly_marker[index].end;
    if(k == 0) {
        poly_terms[poly_marker[avail-1].end + 1].coef = 0;
        poly_terms[poly_marker[avail-1].end + 1].expo = 0;
        poly_marker[avail].start = poly_marker[avail].end = poly_marker[avail-1].end + 1;
        ++avail;
        return avail-1;
    }
    int startRes = poly_marker[avail-1].end + 1;
    int endRes = startRes + (poly_marker[index].end - poly_marker[index].start);
    poly_marker[avail].start = startRes;
    poly_marker[avail].end = endRes;
    ++avail;
    while(start <= end) {
        poly_terms[startRes].coef = k * poly_terms[start].coef;
        poly_terms[startRes].expo = poly_terms[start].expo;
        ++startRes;
        ++start;
    }
    return avail-1;
}

void menu() {
    poly_marker[0].start = 0;
    poly_marker[0].end = 0;
    avail = 1;
    do {
        printf("Enter option: \n");
        printf("  1. Enter two polynomials and add\n");
        printf("  2. Enter two polynomials and subtract\n");
        printf("  3. Enter a polynomial and a constant and multiply the polynomial with the constant\n");
        printf("  4. Check if a polynomial is a zero polynomial\n");
        printf("  5. Find the degree of a polynomial\n");
        printf("  6. Exit\n>");
        int choice;
        scanf("%d", &choice);
        switch(choice) {
            case 1: {
                int poly1, poly2;
                printf("Enter Polynomial 1:");
                poly1 = input_poly();
                printf("Enter Polynomial 2:");
                poly2 = input_poly();
                printf("Polynomial 1:");
                print_poly(poly1);
                printf("Polynomial 2:");
                print_poly(poly2);
                int res_add = add_poly(poly1, poly2);
                printf("Result of adding Polynomial 1 and Polynomial 2:");
                print_poly(res_add);
                break;
            }
            case 2: {
                int poly1, poly2;
                printf("Enter Polynomial 1:");
                poly1 = input_poly();
                printf("Enter Polynomial 2:");
                poly2 = input_poly();
                printf("Polynomial 1:");
                print_poly(poly1);
                printf("Polynomial 2:");
                print_poly(poly2);
                int res_sub = sub_poly(poly1, poly2);
                printf("Result of subtracting Polynomial 2 from Polynomial 1:");
                print_poly(res_sub);
                break;
            }
            case 3: {
                int poly1;
                printf("Enter Polynomial 1:");
                poly1 = input_poly();
                int k;
                printf("Enter constant: ");
                scanf("%d", &k);
                int mult_res = mult_const(poly1, k);
                printf("Result of multiplying %d with Polynomial 1:", k);
                print_poly(mult_res);
                break;
            }
            case 4: {
                int poly1;
                printf("Enter Polynomial 1:");
                poly1 = input_poly();
                break;
            }
            case 5: {
                int poly1;
                printf("Enter Polynomial 1:");
                poly1 = input_poly();
                printf("Degree of the polynomial: %d\n", poly_terms[poly_marker[poly1].start].expo);
                break;
            }
            case 6:
                return;
            default:
                return;
        }
    } while(1);
}

int main() {
    // int startA = -1, startB = -1, endA = 0, endB = 0;
    // int avail = 0;
    // input_poly(&startA, &endA, &avail);
    // input_poly(&startB, &endB, &avail);
    // int startAdd = -1, endAdd = 0;
    // add_poly(startA, endA, startB, endB, &avail, &startAdd, &endAdd);
    // printf("After Addition:");
    // print_poly(startAdd, endAdd);
    // int startSub = -1, endSub = 0;
    // sub_poly(startA, endA, startB, endB, &avail, &startSub, &endSub);
    // printf("After Subtraction:");
    // print_poly(startSub, endSub); 
    menu();
    return 0;
}


/* Input:
3 4 4 3 3 2 2 5 5 5 4 4 3 3 2 2 1 1
*/