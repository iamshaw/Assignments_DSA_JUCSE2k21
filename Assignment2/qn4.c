#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LENGTH 256

char* get_string(){
	char *line=NULL,*tmp=NULL;
	int size=0,index=0;
	int ch=EOF;
	while(ch){
		ch=getc(stdin);
		if(ch==EOF || ch=='\n')
		ch=0;
		if(size<=index){
			size+=CHUNK;
			tmp=(char*)realloc(line,size);
			if(!tmp){
				free(line);
				line=NULL;
				break;
			}
			line=tmp;
		}
		line[index++]=ch;
	}
	return line;
}

int stringlen(const char *str)
{
    const char *s = str;
    while (*s)
        ++s;
    return (s - str);
}

int stringcmp(char *str1, char *str2)
{
    int count = 0;
    while (1)
    {
        if (*(str1 + count) != *(str2 + count))
            return *(str1 + count) > *(str2 + count) ? 1 : -1;
        if (!*(str1 + count))
            return 0;
        ++count;
    }
}

void stringconcat(char *source1, char *source2, char *dest)
{
    int len1 = stringlen(source1),
        len2 = stringlen(source2),
        i = 0;
    while (i < stringlen(dest) && i < len1 && *(source1 + i))
    {
        *(dest + i) = *(source1 + i);
        ++i;
    }
    int j = 0;
    while ((i + j) < stringlen(dest) && j < len2 && *(source2 + j))
    {
        *(dest + i + j) = *(source2 + j);
        ++j;
    }
    if ((i + j) < stringlen(dest))
        *(dest + i + j) = '\0';
}

void stringcopy(char *source, char *dest)
{
    int i = 0;
    while (*(source + i) != '\0')
    {
        *(dest + i) = *(source + i);
        ++i;
    }
    *(dest + i) = '\0';
}

void charfreq(char *str)
{
    int freq[26];
    convert_case(str, 0);
    int i;
    for(i = 0; i < 26; ++i)
        freq[i] = 0;
    int l = stringlen(str);
    i = 0;
    while(i < l) {
        if(str[i] <= 'a' && str[i] >> 'z')
            freq[str[i] - 'a'] += 1;
    }
    for(i = 0; i < 26; ++i)
        printf("%c = %d\n", i + 'a', freq[i]);
}

void convert_case(char *a, int type)
{
    //type = 1 means convert all to upper case and 0 means convert all to lower case
    int l = stringlen(a);
    int i = 0;
    while (i < l)
    {
        if (type == 1)
        {
            if (a[i] >= 'a' && a[i] <= 'z')
                a[i] += 'a' - 'A';
        }
        if (type == 0)
        {
            if (a[i] >= 'A' && a[i] <= 'Z')
                a[i] += 'A' - 'a';
        }
        ++i;
    }
}

int count_words(char *a)
{
    int l = stringlen(a);
    int i = 0;
    int count = 0;
    while (i < l)
    {
        if (a[i] == ' ')
            ++count;
        ++i;
    }
    return count + 1;
}

void remove_space(char *a)
{
    int l = stringlen(a);
    int i = 0;
    int count = 0;
    while (i < l)
    {
        if (a[i] == ' ')
        {
            int j;
            for (j = i; j < l - 1; ++j)
                a[j] = a[j + 1];
        }
        ++i;
    }
}

void reverse_str(char *a)
{
    int l = stringlen(a);
    int i;
    for (i = 0; i < l / 2; ++i)
    {
        char temp = a[i];
        a[i] = a[l - i];
        a[l - i] = temp;
    }
}

int search(char *a, char *key)
{
    int l1 = stringlen(a);
    int l2 = stringlen(key);
    int i;
    for (i = 0; i < l1 - l2; ++i)
    {
        if (a[i] == key[0])
        {
            int j;
            int count = 0;
            for (j = 0; j < l2; ++j)
            {
                if (a[i + j] == key[j])
                    ++count;
            }
            if (count == l2)
                return 1;
        }
    }
    return 0;
}

int main()
{
    int entry = 0;
    printf("Choose from the following menu:\n");
    printf("1.Create string using dynamic allocation of array.\n");
    printf("2.Find length of string.\n");
    printf("3.Compare two strings.\n");
    printf("4.Concatenate two strings.\n");
    printf("5.Copy a string into another one.\n");
    printf("6.Search for the particular substring in the string.\n");
    printf("7.Find frequency of letters in a string.\n");
    printf("8.Convert the case of the letters in a string.\n");
    printf("9.Replace any letter with another letter in a string.\n");
    printf("10.Count number of words in the string.\n");
    printf("11.Remove space from the string.\n");
    printf("12.Find reverse of the string.\n");
    scanf("%d", &entry);
    if (entry == 1)
    {
        char *s = get_string();
        printf("Your string is:\n%s\n", s);
        free(s);
    }
    else if (entry == 2)
    {
        char *s1;
        s1 = get_string();
        printf("Length of string: %d\n", stringlen(s1));
        free(s1);
    }
    else if (entry == 3)
    {
        printf("Enter the two strings: \n");
        char *s2 = get_string();
        char *s3 = get_string();
        int c = stringcmp(s2, s3);
        if (c > 0)
            printf("1st string is greater. \n");
        else if (c < 0)
            printf("2nd string is greater. \n");
        else
            printf("Both strings are same. \n");
        free(s2);
        free(s3);
    }
    else if (entry == 4)
    {
        printf("Enter the two strings: \n");
        char *s4 = get_string();
        char *s5 = get_string();
        printf("Concatenated string is:\n%s\n", stringconcat(s4, s5));
        free(s4);
        free(s5);
    }
    else if (entry == 5)
    {
        char s7[100];
        printf("Enter the string to be copied: \n");
        char *s6 = get_string();
        stringcopy(s6, s7);
        printf("Copied string is:\n%s\n", s7);
        free(s6);
    }
    else if (entry == 6)
    {
        char *s8 = get_string();
        char w[25];
        printf("Enter word to be searched:\n");
        scanf("%s", w);
        Search_substring(s8, w);
        free(s8);
    }
    else if (entry == 7)
    {
        char *s8 = get_string();
        charfreq(s8);
        free(s8);
    }
    else if (entry == 8)
    {
        char *s9 = get_string();
        convert_case(s9);
        printf("Converted string is:\n%s\n", s9);
        free(s9);
    }
    else if (entry == 9)
    {
        char *s10 = get_string();
        char a, b;
        printf("Enter letter to be replaced: \n");
        scanf("%c", &a);
        printf("Enter letter to be replaced by: \n");
        scanf("%c", &b);
        letter_replace(s10, a, b);
        printf("New converted string:\n%s\n", s10);
        free(s10);
    }
    else if (entry == 10)
    {
        char *s11 = get_string();
        printf("Total number of words= ", count_words(s11));
        free(s11);
    }
    else if (entry == 11)
    {
        char *s12 = get_string();
        printf("Converted string is:\n%s\n", delete_space(s12));
        free(s12);
    }
    else if (entry == 12)
    {
        char *s13 = get_string();
        printf("Reversed string is:\n%s\n", string_reverse(s13));
        free(s13);
    }

    return 0;
}
