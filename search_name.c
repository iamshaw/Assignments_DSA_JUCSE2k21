//Problem 3 of assignment 1
#include <stdio.h>

#define MAX_LENGTH 27
#define MAX_NAMES 54

int len_str(char *arr) {
    //  This function returns the length of arr
    int count = 0;
    while(arr[count] != '\n')
        ++count;
    return count;
}

int isalphabet(char a) {
    // This function returns the ASCII value of small case of any alphabet
    // and return -1 if a is not an alphabet
    if (a >= 'A' && a <= 'Z') {
        a = a + ('a' - 'A');
        return a;
    }
    if (a >= 'a' && a <= 'z')
        return a;
    return -1;
}

int cmp_str(char *text1, char *text2) {
    /*  This function returns:
     *      1 if text1 > text2
     *     -1 if text1 < text2
     *      0 if text1 = text2
     */
    int len1 = len_str(text1),
        len2 = len_str(text2);
    int x = 0, y = 0;
    while(text1[x] != '\n' && text2[y] != '\n') {
        int ascii_1, ascii_2;
        do {
            ascii_1 = isalphabet(text1[x]);
        } while(isalphabet(text1[x++]) < 0);
        do {
            ascii_2 = isalphabet(text2[y]);
        } while(isalphabet(text2[y++]) < 0);
        if(ascii_1 != -1 && ascii_2 != -1) {
            if(ascii_1 < ascii_2)
                return -1;
            else if(ascii_1 > ascii_2)
                return 1;
        }
        
    }
    return 0;
}

int linear_search(char **arr, int size, char *element) {
    for(int i = 0; i < size; ++i)
        if(cmp_str(arr[i], element) == 0)
            return i;
    return -1;
}

int binary_search(char **arr, int left, int right, char *element) {
    if(left > right)
        return -1;
    int mid = (left + right) / 2;
    if(cmp_str(arr[mid], element) == 0)
        return mid;
    else if(cmp_str(arr[mid], element) == -1)
        return binary_search(arr, mid, right, element);
    else
        return binary_search(arr, left, mid, element);
}

int main() {
    FILE *fp = fopen("./namelist.txt", "r");
    char namelist[MAX_NAMES][MAX_LENGTH], c;
    int i = 0;
    printf("Names are:\n\n");
    while (fgets(namelist[i], MAX_LENGTH, fp) != NULL) {
        printf("%s", namelist[i]);
        ++i;
    }
    fclose(fp);
    printf("Searching for \"Prasenjit Kumar Shaw\":\n");
    printf("Linear Search gives position: %d", linear_search(namelist, MAX_NAMES - 1, "Prasenjit Kumar Shaw"));
    printf("Binary Search gives position: %d", binary_search(namelist, 0, MAX_NAMES - 1, "Prasenjit Kumar Shaw"));
    return 0;
}