#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

typedef struct node {
    int num;
    struct node *next;
} Node;

void init_l(Node *cur) {
    cur = NULL;
}

bool empty_l(Node *head) {
    return (head == NULL);
}

bool atend_l(Node *cur) {
    if(cur == NULL)
        return false;
    return (cur->next == NULL);
}

void insert_front(Node *target, Node **head) {
    target->next = *head;
    *head = target;
}

void insert_after(Node *target, Node *prev) {
    target->next = prev->next;
    prev->next = target;
}

void delete_front(Node **head) {
    Node *temp = *head;
    *head = (*head)->next;
    temp->next = NULL;
    free(temp);
}

void delete_after(Node *prev) {
    if(!atend_l(prev)) {
        Node *temp = prev->next;
        prev->next = temp->next;
        temp->next = NULL;
        free(temp);
    }
}

void traverse_l(Node *head) {
    int ctr = 0;
    Node *temp = head;
    while(!atend_l(temp)) {
        ++ctr;
        printf("%d-->", temp->num);
        temp = temp->next;
    }
    printf("%d\n", temp->num);
    printf("Number of nodes: %d.\n", ctr);
}

void append_l(Node *head1, Node *head2, Node *target) {
    if(empty_l(head1)) {
        target = &head2;
        return;
    }
    if(empty_l(head2)) {
        target = &head1;
        return;
    }
    Node *temp1 = head1;
    Node *temp2 = target;
    insert_front(temp1, &target);
    while(!atend_l(temp1)) {
        insert_after(temp1, temp2);
        temp1 = temp1->next;
        temp2 = temp2->next;
    }
    temp1 = head2;
    while(!atend_l(temp1)) {
        insert_after(temp1, temp2);
        temp1 = temp1->next;
        temp2 = temp2->next;
    }

}

int main(void) {
    Node *head1 = (Node *) malloc(sizeof(Node));
    printf("For list 1: Enter positive integers: (enter -1 to end inserting nodes)\t>");
    int data = 0;
    Node *prev = NULL;
    int count = 0;
    while(data != -1) {
        ++count;
        Node *temp = (Node *) malloc(sizeof(Node));
        scanf("%d", &data);
        if(data == -1)
            break;
        temp->num = data;
        temp->next = NULL;
        if(count == 1) {
            insert_front(temp, &head1);
            prev = head1;
        }
        else {
            insert_after(temp, prev);
            prev = prev->next;
        }        
    }
    traverse_l(head1);
    Node *head2 = (Node *) malloc(sizeof(Node));
    printf("For list 2: Enter positive integers: (enter -1 to end inserting nodes)\t>");
    prev = NULL;
    data = 0;
    count = 0;
    while(data != -1) {
        ++count;
        Node *temp = (Node *) malloc(sizeof(Node));
        scanf("%d", &data);
        if(data == -1)
            break;
        temp->num = data;
        temp->next = NULL;
        if(count == 1) {
            insert_front(temp, &head2);
            prev = head2;
        }
        else {
            insert_after(temp, prev);
            prev = prev->next;
        }        
    }
    traverse_l(head2);
    Node *target = NULL;
    init_l(target);
    append_l(head1, head2, target);
    traverse_l(target);
    return 0;
}