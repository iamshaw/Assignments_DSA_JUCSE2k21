#include "linked_list.hpp"

void init_l(Node *cur) {
    cur = NULL;
}

bool empty_l(Node *head) {
    return (head == NULL);
}

bool atend_l(Node *cur) {
    if(cur == NULL)
        return false;
    return (cur->next == NULL);
}

void insert_front(Node *target, Node **head) {
    target->next = *head;
    *head = target;
}

void insert_after(Node *target, Node *prev) {
    target->next = prev->next;
    prev->next = target;
}

void delete_front(Node **head) {
    Node *temp = *head;
    *head = (*head)->next;
    temp->next = NULL;
    free(temp);
}

void delete_after(Node *prev) {
    if(!atend_l(prev)) {
        Node *temp = prev->next;
        prev->next = temp->next;
        temp->next = NULL;
        free(temp);
    }
}

void traverse_l(Node *head) {
    Node *temp = head;
    while(!atend_l(temp)) {
        printf("%d-->", temp->num);
        temp = temp->next;
    }
    printf("%d\n", temp->num);
}