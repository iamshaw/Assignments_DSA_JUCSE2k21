#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

typedef struct node {
    int num;
    struct node *next;
} Node;

void init_l(Node *cur) {
    cur = NULL;
}

bool empty_l(Node *head) {
    return (head == NULL);
}

bool atend_l(Node *cur) {
    if(cur == NULL)
        return false;
    return (cur->next == NULL);
}

void insert_front(Node *target, Node **head) {
    target->next = *head;
    *head = target;
}

void insert_after(Node *target, Node *prev) {
    target->next = prev->next;
    prev->next = target;
}

void delete_front(Node **head) {
    Node *temp = *head;
    *head = (*head)->next;
    temp->next = NULL;
    free(temp);
}

void delete_after(Node *prev) {
    if(!atend_l(prev)) {
        Node *temp = prev->next;
        prev->next = temp->next;
        temp->next = NULL;
        free(temp);
    }
}

void traverse_l(Node *head) {
    int ctr = 0;
    Node *temp = head;
    while(!atend_l(temp)) {
        ++ctr;
        printf("%d-->", temp->num);
        temp = temp->next;
    }
    printf("%d\n", temp->num);
    printf("Number of nodes: %d.\n", ctr);
}

int main(void) {
    Node *head = (Node *) malloc(sizeof(Node));
    printf("Enter positive integers: (enter -1 to end inserting nodes)\t\t>");
    int data = 0;
    Node *prev = NULL;
    int count = 0;
    while(data != -1) {
        ++count;
        Node *temp = (Node *) malloc(sizeof(Node));
        scanf("%d", &data);
        if(data == -1)
            break;
        temp->num = data;
        temp->next = NULL;
        if(count == 1) {
            insert_front(temp, &head);
            prev = head;
        }
        else {
            insert_after(temp, prev);
            prev = prev->next;
        }        
    }
    traverse_l(head);
    return 0;
}