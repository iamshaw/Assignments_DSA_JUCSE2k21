#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

typedef struct node {
    int num;
    struct node *next;
} Node;

void init_l(Node *cur);

bool empty_l(Node *head);

bool atend_l(Node *cur);

void insert_front(Node *target, Node **head);

void insert_after(Node *target, Node *prev);

void delete_front(Node **head);

void delete_after(Node *prev);

void traverse_l(Node *head);