#include "linked_list.hpp"

int main(void) {
    Node *head;
    init_l(head);
    Node *A = (Node *) malloc(sizeof(Node));
    A->num = 3;
    A->next = NULL;
    insert_front(A, &head);
    traverse_l(head);
    Node *B = (Node *) malloc(sizeof(Node));
    B->num = 4;
    B->next = NULL;
    insert_front(B, &head);
    traverse_l(head);
    Node *C = (Node *) malloc(sizeof(Node));
    C->num = 5;
    C->next = NULL;
    insert_after(C, head);
    traverse_l(head);
    printf("----\n");
    delete_front(&head);
    traverse_l(head);
    delete_after(head);
    traverse_l(head);

    return 0;
}