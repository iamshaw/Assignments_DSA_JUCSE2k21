#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

typedef struct node {
    int num;
    struct node *next;
} Node;

void init_l(Node *cur) {
    cur = NULL;
}

bool empty_l(Node *head) {
    return (head == NULL);
}

bool atend_l(Node *cur) {
    if(cur == NULL)
        return false;
    return (cur->next == NULL);
}

void insert_front(Node *target, Node **head) {
    target->next = *head;
    *head = target;
}

void insert_after(Node *target, Node *prev) {
    target->next = prev->next;
    prev->next = target;
}

void delete_front(Node **head) {
    Node *temp = *head;
    *head = (*head)->next;
    temp->next = NULL;
    free(temp);
}

void delete_after(Node *prev) {
    if(!atend_l(prev)) {
        Node *temp = prev->next;
        prev->next = temp->next;
        temp->next = NULL;
        free(temp);
    }
}

void traverse_l(Node *head) {
    if(!atend_l(head)) {
        printf("%d", head->num);
    }
    Node *temp = head->next;
    while(!atend_l(temp)) {
        printf("-->%d", temp->num);
        temp = temp->next;
    }
    printf("\n");
}

int main(void) {
    FILE *fp = fopen("qn2_Input.txt", "r");
    if(fp == NULL) {
        printf("Unable to open file...\n");
        exit(1);
    }
    int choice;
    printf("Select read mode:\t1. As is.\t\t2. Reverse.\t\t>");
    scanf("%d", &choice);
    if(choice == 1) {
            int data;
            Node *head = (Node *) malloc(sizeof(Node));
            Node *pointer = NULL;
            init_l(head);
            if(!feof(fp)) {
                fscanf(fp, "%d\n", &data);
                Node *temp = (Node *) malloc(sizeof(Node));
                temp->num = data;
                temp->next = NULL;
                insert_front(temp, &head);
            }
            pointer = head;
            while(!feof(fp)) {
                fscanf(fp, "%d\n", &data);
                Node *temp = (Node *) malloc(sizeof(Node));
                temp->num = data;
                temp->next = NULL;
                insert_after(temp, pointer);
                pointer = pointer->next;
            }
            traverse_l(head);
        }
    else if(choice == 2) {
        int data;
        Node *head = (Node *) malloc(sizeof(Node));
        init_l(head);
        while(!feof(fp)) {
            fscanf(fp, "%d\n", &data);
            Node *temp = (Node *) malloc(sizeof(Node));
            temp->num = data;
            temp->next = NULL;
            insert_front(temp, &head);
        }
        traverse_l(head);
    }
    else {
        printf("Wrong Choice...\n");
        fclose(fp);
        exit(1);
    }
    fclose(fp);
    return 0;
}
