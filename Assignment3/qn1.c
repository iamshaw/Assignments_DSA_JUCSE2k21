#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

typedef struct node {
    int num;
    struct node *next;
} Node;

void init_l(Node *cur) {
    cur = NULL;
}

bool empty_l(Node *head) {
    return (head == NULL);
}

bool atend_l(Node *cur) {
    if(cur == NULL)
        return false;
    return (cur->next == NULL);
}

void insert_front(Node *target, Node **head) {
    target->next = *head;
    *head = target;
}

void insert_after(Node *target, Node *prev) {
    target->next = prev->next;
    prev->next = target;
}

void delete_front(Node **head) {
    Node *temp = *head;
    *head = (*head)->next;
    temp->next = NULL;
    free(temp);
}

void delete_after(Node *prev) {
    if(!atend_l(prev)) {
        Node *temp = prev->next;
        prev->next = temp->next;
        temp->next = NULL;
        free(temp);
    }
}

void traverse_l(Node *head) {
    Node *temp = head;
    while(!atend_l(temp)) {
        printf("%d-->", temp->num);
        temp = temp->next;
    }
    printf("%d\n", temp->num);
}

int main(void) {
    Node *head;
    init_l(head);
    Node *A = (Node *) malloc(sizeof(Node));
    A->num = 3;
    A->next = NULL;
    insert_front(A, &head);
    traverse_l(head);
    Node *B = (Node *) malloc(sizeof(Node));
    B->num = 4;
    B->next = NULL;
    insert_front(B, &head);
    traverse_l(head);
    Node *C = (Node *) malloc(sizeof(Node));
    C->num = 5;
    C->next = NULL;
    insert_after(C, head);
    traverse_l(head);
    printf("----\n");
    delete_front(&head);
    traverse_l(head);
    delete_after(head);
    traverse_l(head);
    return 0;
}