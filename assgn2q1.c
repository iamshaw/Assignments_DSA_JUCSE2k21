#include <stdio.h>

typedef struct term {
    int expo;
    float coeff;
} TERM;

typedef struct poly {
    int beg;
    int end;
} POLY;

void enter_exp(TERM *arr, int term_size, POLY *ctr, int *curr_pol_pos, int poly_size) {
    /*
    TERM *arr is pointer to the array storing the polynomial.
    term_size is the max size of the *arr.
    POLY *ctr is the array string the beginning and ending positions of each polynomial.
    *curr_pol_pos is the position of last filled entry of *ctr.
    poly_size is the max size of *ctr.
    */
    int exp;
    float coef;
    printf("Enter highest exponent: ");
    scanf("%d", &exp);
    if (*(ctr + *curr_pol_pos + exp)->end >= term_size) {
        printf("Polynomial array is full. Can't add any more polynomials.\n");
        return;
    }
    if (curr_pol_pos == 0)      // No polynomials present in *arr
        *(ctr)->beg = 0;
    else
        *(ctr + *curr_pol_pos + 1)->beg = *(ctr + *curr_pol_pos)->end + 1;
    int counter = 0;
    while (exp >= 0) {
        printf("  Enter coefficient for x^%d: ", exp);
        scanf("%f", &coef);
        if (coef) {
            ++counter;
            *(arr + *(ctr + *curr_pol_pos)->end + counter)->expo = exp;
            *(arr + *(ctr + *curr_pol_pos)->end + counter)->coeff = coef;
        }
        --exp;
    }
    *(ctr + *curr_pol_pos + 1)->end = *(ctr + *curr_pol_pos)->end + counter;
    ++(*curr_pol_pos);
}

void print_poly(TERM *arr, POLY *ctr, int curr_pol_pos) {
    int poly_num = 0;
    // int 
}