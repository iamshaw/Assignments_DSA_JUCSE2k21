#include <stdio.h>
#include <stdlib.h>

typedef struct node {
    int num;
    struct node *next;
} List;

List *create_node(int n) {
    List *hd = NULL;
    if((hd = (List *) malloc(sizeof(List))) == NULL)
        perror("MALLOC ERROR!");
    else {
        hd->num = n;
        hd->next = NULL;
    }
    return hd;
}

void init_l(List *cur) {
    cur = NULL;
}

void insert_front(List *target, List **phead) {
    target->next = *phead;
    *phead = target;
}

void traverse(List *head) {
    List *temp = head;
    printf("Traversing the list:\n");
    if(temp == NULL) {
        printf("List is empty.\n");
        return;
        }
/*    while (temp !=NULL) {
        printf("%d  ", temp->num);
        temp= temp->next;
        } */
     while(temp->next != NULL) {
        printf("%d->", temp->num);
        temp = temp->next;
    }
    printf("%d\n---\n", temp->num);

}

void insert_after(List *target, List *prev) {
    target->next = prev->next;
    prev->next = target;

}

int at_end(List *cur) {
    if(cur == NULL)
        return 1;
    return 0;
}

int empty_list(List *head) {
    if(head == NULL)
        return 1;
    return 0;
    }
    
void delete_front(List **phead) {
    List *temp = *phead;
    *phead = (*phead)->next;
    free(temp);
}

void delete_after(List *prev) {
    List *temp = prev->next;
    prev->next = temp->next;
    free(temp);
}

void read_file(FILE *fp, List **head, int order) {
     List *lt = *head;
    while(!feof(fp)) {
        int temp;
        fscanf(fp, "%d", &temp);
        printf("%d  ", temp);
        List *tmp = create_node(temp);
        if(tmp == NULL){printf("ERROR");}
        if(order == 1)
            insert_front(tmp, head);
        else if (order == 2) {
            insert_front(tmp, head);
            ++order;
            //lt = lt->next;
         }
        else {
            // lt = lt->next;
            printf("working\n");
            insert_after(tmp, lt);
                         
        }
    }
}

int main() {
    FILE *fp = fopen("Assign3Prob2_INPUT.txt", "r");
    if(fp == NULL) {
        printf("ERROR!!! File not found.\n");
        return 0;
    }
    List *head = NULL;
    init_l(head);
    int order = 1;
    printf("How to store the file?\n1. Reverse Order\t2. As is\n>");
    scanf("%d", &order);
    while (order != 1 && order != 2) {
        printf("Wrong option. Enter proper option: ");
        scanf("%d", &order);
    }
    read_file(fp, &head, order);
    printf("Traversing the List:\n");
    traverse(head);
    return 0;
}
