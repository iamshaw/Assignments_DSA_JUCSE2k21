#include <stdio.h>
#include <stdlib.h>

typedef struct node {
    int num;
    struct node *next;
} List;

List *create_node(int n) {
    List *hd = NULL;
    if((hd = (List *) malloc(sizeof(List))) == NULL)
        perror("MALLOC ERROR!");
    else {
        hd->num = n;
        hd->next = NULL;
    }
    return hd;
}

void init_l(List *cur) {
    cur = NULL;
}

void insert_front(List *target, List **phead) {
    target->next = *phead;
    *phead = target;
}

void traverse(List *head) {
    List *temp = head;
    printf("Traversing the list:\n");
    if(temp == NULL) {
        printf("List is empty.\n");
        return;
        }
/*    while (temp !=NULL) {
        printf("%d  ", temp->num);
        temp= temp->next;
        } */
     while(temp->next != NULL) {
        printf("%d->", temp->num);
        temp = temp->next;
    }
    printf("%d\n---\n", temp->num);

}

void insert_after(List *target, List *prev) {
    target->next = prev->next;
    prev->next = target;
}

int at_end(List *cur) {
    if(cur == NULL)
        return 1;
    return 0;
}

int empty_list(List *head) {
    if(head == NULL)
        return 1;
    return 0;
    }
    
void delete_front(List **phead) {
    List *temp = *phead;
    *phead = (*phead)->next;
    free(temp);
}

void delete_after(List *prev) {
    List *temp = prev->next;
    prev->next = temp->next;
    free(temp);
}

int main() {
    List *head = NULL;
    init_l(head);
    traverse(head);
    List *a = create_node(5);
    insert_front(a, &head);
    traverse(head);
    List *b = create_node(4);
    insert_front(b, &head);
    traverse(head);
    List *c = create_node(3);
    insert_after(c, (head));
    traverse(head);
    delete_front(&head);
    traverse(head);
    delete_after(head);
    traverse(head);
    
    return 0;
}
