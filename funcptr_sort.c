#include <stdio.h>

int grtr_than(int a, int b) {
	return a > b;
}

int less_than(int a, int b) {
	return a < b;
}

void sort(int *arr, int size, int (*compare)(int, int)) {
	int i = 0,
	    j = 0,
	    temp = 0,
	    pos = 0;
	for(i = 0; i < size-1; ++i) {
		pos = i;
		for(j = i; j < size; ++j)
			if((*compare)(*(arr + j), *(arr + pos)))
				pos = j;
		temp = *(arr + pos);
		*(arr + pos) = *(arr + i);
		*(arr + i) = temp;
	}
}

int main() {
	int size = 5;
	int arr[5] = {3, 4, 1, 2, 5};
	int i = 0;
	printf("The array is:\n");
	for(; i < size; ++i)
		printf("%d ", *(arr + i));
	printf("\n");

	int (*sort_asc)(int, int);
	sort_asc = &less_than;
	int (*sort_desc)(int, int);
	sort_desc = &grtr_than;
	
	printf("Sorting the array in ascending order:\n");
	sort(arr, size, sort_asc);
	for(i = 0; i < size; ++i)
		printf("%d ", *(arr + i));
	printf("\n");

	printf("Sorting the array in descending order:\n");
	sort(arr, size, sort_desc);
	for(i = 0; i < size; ++i)
		printf("%d ", *(arr + i));
	printf("\n");

	return 0;
}
