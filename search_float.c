//Problem 3 of assignment 1
#include <stdio.h>

int linear_search(float arr[], int size, float element) {
    for(int i = 0; i < size; ++i)
        if(arr[i] == element)
            return i;
    return -1;
}

int binary_search(float arr[], int left, int right, float element) {
    if(left > right)
        return -1;
    int mid = (left + right) / 2;
    if(arr[mid] == element)
        return mid;
    else if(arr[mid] < element)
        return binary_search(arr, mid, right, element);
    else
        return binary_search(arr, left, mid, element);
}

int main() {
    float arr[] = {1.2, 3.3, 4.1, 5.99, 6.31, 7.21, 9.3, 10.0};
    int size = 8;
    printf("Searching for 9.3:\n");
    printf("Linear search gives position: %d\n", linear_search(arr, size, 9.3));
    printf("Binary search gives position: %d\n", binary_search(arr, 0, size - 1, 9.3));
    return 0;
}