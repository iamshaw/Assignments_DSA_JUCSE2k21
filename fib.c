#include <stdio.h>
#include <time.h>

#define UPPER 45

long RECURSSION_CALLS = 0;

long fib(int n) {
	++RECURSSION_CALLS;
	if (n <= 0)
		return 0;
	if (n == 1)
		return 1;
	return fib(n - 1) + fib(n - 2);
}

int main() {
	time_t start_loop, end_loop, start_rec, end_rec;
	double loop_time, rec_time;
	int a, b, c, n, temp;
	long loop_result, rec_result;

	printf("n,loop_result,loop_time,rec_result,rec_time,RECURSSION_CALLS\n");

	for( n = 1; n <= UPPER; ++n) {
		start_loop = time(NULL);
		temp = n;
		a = 0;
		b = 1;
		while (temp--) {
			c = a + b;
			a = b;
			b = c;
		}
		loop_result = a;
		end_loop = time(NULL);

		start_rec = time(NULL);
		RECURSSION_CALLS = 0;
		rec_result = fib(n);
		end_rec	 = time(NULL);

		loop_time = (double)(end_loop - start_loop);
		rec_time = (double)(end_rec - start_rec);
		printf("%d,%ld,%lf,%ld,%lf,%ld\n", n, loop_result, loop_time, rec_result, rec_time, RECURSSION_CALLS);
	}
	return 0;
}