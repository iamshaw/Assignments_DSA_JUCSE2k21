#include <stdio.h>

long iter = 0;


void init_board(int *board, int n) {
    int i;
    for(i = 0; i < n; ++i)
        board[i] = -1;
}

int check_validity(int *board, int n, int row) {
    int i;
    for(i = 0; i < row; ++i) {
        int pos = board[i];
        if(board[row] == pos) {
            printf("board[%d] == %d  which is invalid\n", row, pos);
            return 0;
        }
        int left = pos - 1;
        int right = pos + 1;
        int j;
        for(j = i + 1; j < row - 1; ++j) {
            if(board[j] == left  && left >=0)  {
                printf("board[%d] is in path\n", j);
                return 0;
            }
            if(board[j] == right && right < n) {
                printf("board[%d] is in path\n", j);
                return 0;
            }
            if(left > 0)
                ++left;
            if(right < n - 1)
                ++right;
        }
    }
    return 1;
}

void print_board(int *board, int n) {
    ++iter;
    int i, j;
    printf("\n");
    for(i = 0; i < n; ++i) {
        for(j = 0; j < n; ++j) {
            // printf("-----");
            if(j == board[i])
                printf("| Q |");
            else
                printf("|   |");
        }
        printf("\n");
        // printf("-----");
    }
    printf("\n");
}

int n_queens(int *board, int n, int row) {
    int i;
    for(i = 0; i < n; ++i) {
        board[row] = i;
        printf("board[%d] = %d\n", row, i);
        print_board(board, n);
        if(check_validity(board, n, row)) {
            if(row == n)
                return 1;
            else {
                int check = n_queens(board, n, row + 1);
                if(check == 1) {
                    return 1;
                }
            }
        }
    }
    board[row] = -1;
    return 0;
}

int main() {
    int n;
    printf("Enter the number of queens: ");
    scanf("%d", &n);
    int board[n];
    init_board(board, n);
    int solved = n_queens(board, n, 0);
    if(solved)
        print_board(board, n);
    else
        printf("Can't solve... Sorry.\n");
    printf("iter = %ld\n", iter);
    return 0;
}