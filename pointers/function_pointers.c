#include <stdio.h>

void my_func(int x) {
    printf("%d\n", x);
}

int main() {
    void (*foo)(int);
    foo = &my_func;
    foo(2);
    (*foo)(3);
    return 0;
}