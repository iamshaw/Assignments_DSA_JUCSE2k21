#include <stdio.h>

main() {
    int p[3][5] = {
        {2,4,6,8,10},
        {3,6,9,12,15},
        {5,10,15,20,25}
    };
    printf("%x\n", p);
    printf("%x\n", *p);
    printf("%x\n", *(*p));
    printf("%x\n", *(*p+1));
    printf("%x\n", *(*(p+1)));
    printf("%ld\n", sizeof(&p));
}