#include <stdio.h>

int stringlen(const char *str) {
    const char *s = str;
    while(*s)
        ++s;
    return (s - str);
}

#include <stdio.h>


int stringcmp(char *str1, char *str2) {
    // int temp = 0;
    // while(temp < stringlen(str1)) {
    //     if (*(str1 + temp) != '\0' && *(str2 + temp) != '\0') {        
    //         if (*(str1 + temp) < *(str2 + temp))
    //             return -1;
    //         if (*(str1 + temp) > *(str2 + temp))
    //             return 1;
    //     }
    //     if (*(str1 + temp) == '\0' && *(str2 + temp) != '\0')
    //         return -1;
    //     else if (*(str2 + temp) == '\0')
    //         return 1;
    //     ++temp;
    // }
    // if(stringlen(str1) == stringlen(str2))
    //     return 0;
    // return -1;

    int count = 0;
    while(1) {
        if(*(str1 + count) != *(str2 + count))
            return *(str1 + count) > *(str2 + count) ? 1 : -1;
        if(!*(str1 + count)) 
            return 0;
        ++count;
    }
}

void stringconcat(char *source1, char *source2, char *dest) {
    int len1 = stringlen(source1),
        len2 = stringlen(source2),
        i = 0;
    while(i < stringlen(dest) && i < len1 && *(source1 + i)) {
        *(dest + i) = *(source1 + i);
        ++i;
    }
    int j = 0;
    while((i + j) < stringlen(dest) && j < len2 && *(source2 + j)) {
        *(dest + i + j) = *(source2 + j);
        ++j;
    }
    if((i + j) < stringlen(dest))
        *(dest + i + j) = '\0';
}

void stringcopy(char *source, char *dest) {
    int i = 0;
    while(*(source + i) != '\0') {
        *(dest + i) = *(source + i);
        ++i;
    }
    *(dest + i) = '\0';
}

void charfreq(char *str, int *freq) {
    int temp = 26;
    while(temp)
        freq[temp] = 0;
    int i = 0;
    while(i < stringlen(str)) {
        if(*(str + i) >= 'A' && *(str + i) <= 'Z')
            *(str + i) -= 'A' - 'a';
        ++*(freq + *(str + i) - 'a');
        ++i;
        printf("%c\n", *(str+i) - 'a');
        scanf("%d", &temp);
    }
}

int main() {
    char a[10] = "af";
    char b[10] = "acd";
    printf("a = %s\nb = %s\n", a, b);
    printf("len(b) = %d\n", stringlen(b));
    printf("stringcmp(a, b) = %d\n", stringcmp(a, b));
    char dest[20];
    stringconcat(a, b, dest);
    printf("stringconcat(a, b, dest) = %s\n", dest);
    stringcopy(a, dest);
    printf("stringcopy(a, dest) = %s\n", dest);
    int freq[26], i;
    charfreq("aabcdddddefeklaijsdohasjbciuabcja", freq);
    for(i = 0; i < 26; ++i)
        if(freq[i])
            printf("%c: %d\n", 'a' + i, freq[i]);
    return 0;
}
