#include <stdio.h>

typedef struct Node {
    Node *next;
    int data;
} Node;

Node *init_l() {
    Node *new_node = NULL;
    return new_node;
}

int is_empty(Node *head) {
    if(head != NULL) {
        return 1;
    }
    else
        return 0;
}

int size_l(Node *head) {
    Node *temp = head;
    int size = 0;
    do {
        ++size;
        temp = temp->next;
    } while(temp->next != NULL);
    return size;
}

void insert_front(Node *head, Node *element) {
    element->next = head;
    head = element;
}

void insert_after(Node *prev, Node *element) {
    element->next = prev->next;
    prev->next = element;
}

void delete_next_node(Node *prev) {
    Node *temp = prev->next;
    prev->next = prev->next->next;
    free(temp);
}

Node *last_node(Node *head) {
    Node *temp = head;
    do {
        temp = temp->next;
    } while(temp->next != NULL);
    return temp;
}

void delete_list(Node *head) {
    do {
        Node *temp = head;
        head = head->next;
        free(temp);
    } while(head != NULL);
}

Node *advance(Node *current) {
    current = current->next;
    return current;
}

// void sort_list(Node *head, int element) {
//     if(is_empty(head)) {
//         printf("Linked list is empty.\n");
//         return NULL;
//     }
//     Node *prev = head,
//          *temp = head;
//     int size = size_l(head);
//     for(int i = 0; i < size; ++i) {
//         for(int j = i; j < size; ++j) {
//             ;
//         }
//     }
// }

// Node *reverse_list(Node *head) {
//     Node *temp1 = NULL,
//         *temp2 = NULL,
//         *temp3 = NULL;
//     temp1 = head;
//     temp2 = head->next;
//     head->next = NULL;
//     do {
//         temp3 = temp2->next;
//         // temp->next
//     } while(temp1 != NULL);
//     return head;
// }