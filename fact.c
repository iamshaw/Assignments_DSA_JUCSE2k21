#include <stdio.h>
#include <time.h>

#define UPPER 20

long RECURSSION_CALLS = 0;

long fact(int n) {
	++RECURSSION_CALLS;
	if(n <= 1)
		return 1;	
	return n * fact(n-1);
}

int main() {
	time_t start_loop, end_loop, start_rec, end_rec;
	double loop_time, rec_time;
	int n, temp;
	long loop_result, rec_result;

	printf("n,loop_result,loop_time,rec_result,rec_time,RECURSSION_CALLS\n");

	for( n = 1; n <= UPPER; ++n) {
		start_loop = time(NULL);
		temp = n;
		loop_result = 1;
		while (temp) {
			loop_result = loop_result * temp;
			--temp;
		}
		end_loop = time(NULL);

		start_rec = time(NULL);
		RECURSSION_CALLS = 0;
		rec_result = fact(n);
		end_rec	 = time(NULL);

		loop_time = (double)(end_loop - start_loop);
		rec_time = (double)(end_rec - start_rec);
		printf("%d,%ld,%lf,%ld,%lf,%ld\n", n, loop_result, loop_time, rec_result, rec_time, RECURSSION_CALLS);
	}
	return 0;
}