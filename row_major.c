#include <stdio.h>

int main() {
    int arr[3][3] = {{0, 1, 2}, {3, 4, 5}};
    for(int i = 0; i < 3; ++i) {
        for(int j = 0; j < 3; ++j)
            printf("%x ", &arr[i][j]);
        printf("\n");
    }
    return 0;
}

/*
 * Output:
 *      //ec127bc0 ec127bc4 ec127bc8
 *      //ec127bcc ec127bd0 ec127bd4
 *      //ec127bd8 ec127bdc ec127be0
 *  This proves that C stores array in row major way.
 */