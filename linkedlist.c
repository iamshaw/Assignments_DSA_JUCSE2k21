#include <stdio.h>

typedef struct Node_Info {
    int a;
} NInfo;

typedef struct Nodetag {
    NInfo info;
    struct nodetag *next;
} Node;

Node *createnode (NInfo item) {
    Node *head = NULL;
    if ((head = (Node *) malloc(sizeof(Node))) == NULL)
        perror("malloc error!");
    else {
        head->info = item;
        head->next = NULL;
    }
    return head;
}

Node *init_l () {
    return NULL;
}

int empty_list (Node *head) {
    return (head == NULL);
}

int at_end_l (Node *curr) {
    return (curr == NULL);
}

void insert_front (Node *target, Node **ptr_head) {
    target->next = *ptr_head;
    *ptr_head = target;
}

void insert_after (Node *target, Node *prev) {
    target->next = prev->next;
    prev->next = target;
}

void advance (Node *curr) {
    curr = curr->next;
}

void delete_front (Node *head) {
    Node *temp = head;
    advance(head);
    free(temp);
}

Node *search_ord (NInfo item, Node *head) {
    if (empty_list(head))
        return NULL;
    Node *temp = head;
    while (!at_end_l(temp)) {
        if (temp->info.a == item.a)
            return temp;
        if (temp->info.a > item.a)
            return NULL;
        advance(temp);
    }
    return NULL;
}

Node *search_unord (NInfo item, Node *head) {
    if (empty_list(head))
        return NULL;
    Node *temp = head;
    while (!at_end_l(temp)) {
        if(temp->info.a == item.a)
            return temp;
        advance(temp);
    }
    return NULL;
}

int list_size (Node *head) {
    if (empty_list(head))
        return 0;
    Node *temp = head;
    int counter = 0;
    while (!at_end_l(temp)) {
        ++counter;
        advance(temp);
    }
    return counter;
}

void reverse_list (Node *head) {
    Node *prev = NULL,
         *curr = head,
         *nxt = head->next;
    while (!at_end_l(nxt)) {
        curr->next = prev;
        prev = curr;
        curr = nxt;
        advance(nxt);
    }
    head = nxt;
    head->next = curr;
}