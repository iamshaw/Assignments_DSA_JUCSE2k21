// Problem 5 of 1st assignment
#include <stdio.h>

#define MAX_LENGTH 27
#define MAX_NAMES 54

int len_str(char *arr) {
    //  This function returns the length of arr
    int count = 0;
    while(arr[count] != '\n')
        ++count;
    return count;
}

void copy_str(char *target, char *source) {
    // This function copies content of source to target
    // upto the length of MAX_LENGTH
    for(int i = 0; i < MAX_LENGTH; ++i)
        target[i] = source[i];
}

int* compare(char namelist[][MAX_LENGTH], int size, int pos[2]) {
    //  This function returns a arr of size 2 whose 1st element
    //  is the position of text in namelist having minimum length
    //  and 2nd element is the position of maximum length
    int max_len = 0, min_len = MAX_LENGTH;
    for(int i = 0 ; i < size; ++i) {
        int len = len_str(namelist[i]);
        if(len > max_len) {
            pos[1] = i;
            max_len = len;
        }
        if(len < min_len) {
            pos[0] = i;
            min_len = len;
        }
    }
    return pos;
}

int isalphabet(char a) {
    // This function returns the ASCII value of small case of any alphabet
    // and return -1 if a is not an alphabet
    if (a >= 'A' && a <= 'Z') {
        a = a + ('a' - 'A');
        return a;
    }
    if (a >= 'a' && a <= 'z')
        return a;
    return -1;
}

int cmp_str(char *text1, char *text2) {
    /*  This function returns:
     *      1 if text1 > text2
     *     -1 if text1 < text2
     *      0 if text1 = text2
     */
    int len1 = len_str(text1),
        len2 = len_str(text2);
    int x = 0, y = 0;
    while(text1[x] != '\n' && text2[y] != '\n') {
        int ascii_1, ascii_2;
        do {
            ascii_1 = isalphabet(text1[x]);
        } while(isalphabet(text1[x++]) < 0);
        do {
            ascii_2 = isalphabet(text2[y]);
        } while(isalphabet(text2[y++]) < 0);
        if(ascii_1 != -1 && ascii_2 != -1) {
            if(ascii_1 < ascii_2)
                return -1;
            else if(ascii_1 > ascii_2)
                return 1;
        }
        
    }
    return 0;
}

void sort(char namelist[][MAX_LENGTH], int size) {
    //  This function sorts the elements in namelist
    //  as they would appear in a dictionary
    for(int i = 0; i < size; ++i) {
        for(int j = i + 1; j < size; ++j) {
            char name[MAX_LENGTH];
            copy_str(name, namelist[i]);
            if(cmp_str(namelist[i], namelist[j]) == 1) {
                copy_str(namelist[i], namelist[j]);
                copy_str(namelist[j], name);
            }
        }
    }
}


int main() {
    FILE *fp = fopen("./namelist.txt", "r");
    char namelist[MAX_NAMES][MAX_LENGTH], c;
    int i = 0;
    printf("Names are:\n\n");
    while (fgets(namelist[i], MAX_LENGTH, fp) != NULL) {
        printf("%s", namelist[i]);
        ++i;
    }
    fclose(fp);
    int pos[2];
    compare(namelist, MAX_NAMES, pos);
    printf("Min: %d: %sLength: %d\nMax: %d: %sLength: %d\n",
            pos[0], namelist[pos[0]], len_str(namelist[pos[0]]),
            pos[1], namelist[pos[1]], len_str(namelist[pos[1]]));
    printf("Sorting...\n");
    sort(namelist, MAX_NAMES);
    FILE *f_sorted = fopen("./namelist_sorted.txt", "w");
    for(int i = 0; i < MAX_NAMES; ++i) {
        // printf("%s", namelist[i]);
        fprintf(f_sorted, "%s", namelist[i]);
    }
    fclose(f_sorted);
    printf("Names are sorted. Checkout \"./namelist.txt\".\n");
    return 0;
}