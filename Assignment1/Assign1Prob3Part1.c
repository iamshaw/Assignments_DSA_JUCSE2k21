//Problem 3 of assignment 1
#include <stdio.h>

int linear_search(int arr[], int size, int element) {
    for(int i = 0; i < size; ++i)
        if(arr[i] == element)
            return i;
    return -1;
}

int binary_search(int arr[], int left, int right, int element) {
    if(left > right)
        return -1;
    int mid = (left + right) / 2;
    if(arr[mid] == element)
        return mid;
    else if(arr[mid] < element)
        return binary_search(arr, mid, right, element);
    else
        return binary_search(arr, left, mid, element);
}

int main() {
    int arr[] = {1, 3, 4, 5, 6, 7, 9, 10};
    int size = 8;
    printf("Searching for 7:\n");
    printf("Linear search gives position: %d\n", linear_search(arr, size, 7));
    printf("Binary search gives position: %d\n", binary_search(arr, 0, size - 1, 7));
    return 0;
}