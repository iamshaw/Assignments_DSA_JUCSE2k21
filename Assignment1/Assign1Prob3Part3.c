//Problem 3 of assignment 1
#include <stdio.h>
#include <string.h>

#define MAX_LENGTH 30
#define MAX_NAMES 54



int linear_search(char arr[][15], int size, char element[15]) {
    for(int i = 0; i < size; ++i) {
        // printf("%s\t%d\t%s\n", arr[i], strcmp(arr[i], element), element);
        if(strcmp(arr[i], element) == 0) {
            return i;
        }
    }
    return -1;
}

int binary_search(char arr[][15], int left, int right, char element[15]) {
    if(left > right)
        return -1;
    int mid = (left + right) / 2;
    if(strcmp(arr[mid], element) == 0)
        return mid;
    else if(strcmp(arr[mid], element) < 0)
        return binary_search(arr, mid, right, element);
    else
        return binary_search(arr, left, mid, element);
}

int main() {
    char namelist[][15] = {"aastha", "abhay", "aditya", "chandu", "dj", "milan", "nazif", "shaw"};
    int size = 8;
    for(int i = 0; i < 8; ++i) {
        printf("%d: %s\n", i, namelist[i]);
    }
    char name[] = {"milan"};
    printf("Searching for \"milan\":\n");
    int pos_lin = 0, pos_bin = 0;
    pos_lin = linear_search(namelist, 8, name);
    pos_bin = binary_search(namelist, 0, 7, name);
    printf("Linear Search gives position: %d\n", pos_lin);
    printf("Binary Search gives position: %d\n", pos_bin);
    return 0;
}
