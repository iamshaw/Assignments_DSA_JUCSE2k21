// Problem 7 of assignment 1
#include <stdio.h>
#include <math.h>

int len_str(char *arr) {
    //  This function returns the length of arr
    int count = 0;
    while(arr[count] != '\n')
        ++count;
    return count;
}

int expo(long long int number) {
    long long int temp;
    int count;
    while(temp) {
        ++count;
        temp = temp / 10;
    }
    return count;
}

long long int juxtapose(char* name) {
    int len = len_str(name);
    long long int result = 0;
    for(int i = 0; i < len; ++i) {
        // printf("%c", name[i]);
        result = (result * 100) + (int)name[i];
        // printf("\t%lld\n", result);
    }
    return result;
}

int main() {
    // printf("%ld", sizeof(long long int)/sizeof(int));
    char name[10], sur_name[5];//= {"Prasenjit Shaw"};
    long long int name_int, sur_name_int;
    char c;
    int i = 0;
    do {
        scanf("%c", &c);
        name[i] = c;
        ++i;
    } while((c != '\n') && i < 20);
    i = 0;
    do {
        scanf("%c", &c);
        sur_name[i] = c;
        ++i;
    } while((c != '\n') && i < 20);
    name_int = juxtapose(name);
    sur_name_int = juxtapose(sur_name);
    printf("%s\t%lld\n", name, name_int);
    printf("%s\t%lld\n", sur_name, sur_name_int);
    long long int name1, name2, sur_name1, sur_name2;
    name1 = name_int / (long long int)pow(10, expo(name_int));
    name2 = name_int % (long long int)pow(10, expo(name_int));
    sur_name1 = sur_name_int / (long long int)pow(10, expo(sur_name_int));
    sur_name2 = sur_name_int % (long long int)pow(10, expo(sur_name_int));
    name_int = name1 + name2;
    sur_name_int = sur_name1 + sur_name2;
    printf("Enter 3 4-digit prime numbers:\n");
    int prime[3];
    scanf("%d""%d""%d", &prime[0], &prime[1], &prime[2]);
    for(int i = 0; i < 3; ++i) {
        printf("name: %lld mod %d = %lld\n", name_int, prime[i], name_int % prime[i]);
        printf("sur_name: %lld mod %d = %lld\n", sur_name_int, prime[i], sur_name_int % prime[i]);
    }
    return 0;
}