#include <stdio.h>
#include <stdlib.h>

int main() {
	long arr[60000];
	for(long i = 0; i < 60000; ++i)
		arr[i] = 1;
	long res[60000];
	for (long i = 0; i < 60000; ++i) {
		long pos = 0;
		do {
			pos = rand() % 60000;
		} while (!arr[pos]);
		res[i] = pos + 1;
		arr[pos] = 0;
	}
	for (long i = 0; i < 60000; ++i)
		printf("%ld\n", res[i]);
	return 0;
}